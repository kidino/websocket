<?php 

require dirname(__DIR__) . '/vendor/autoload.php';

if ($_POST) {

    $date = new DateTime(date(''), new DateTimeZone('Asia/Kuala_Lumpur'));

    $data = [
        'action' => 'new',
        'data' => [
            'datetime' => $date->format('Y-m-d H:i:s'),
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'username' => $_POST['username']
        ]
    ];

    /**
     * ------------------------------------------------------
     * Kod untuk menyimpan pengguna ke database boleh dibuat
     * di sini.
     * ------------------------------------------------------
     */


    $client = new WebSocket\Client("wss://updatedb.ws.xmlot.com");
    $client->text( json_encode($data) );
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UPDATE</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>    
</head>
<body>

    <div class="container mt-5">
        <div class="row">
            <div class="col">

                <form action="update-ui.php" method="post" class="rounded col-lg-6 offset-lg-3 p-5 shadow">
                <?php if(isset($_POST['name'])) : ?>
                    <p class="alert alert-success">New user has been registered</p>
                <?php endif; ?>

                <h1 class="mb-4">Registration Page</h1>
                
                <label for="">Name</label>
                    <input type="text" name="name" class="form-control mb-3">

                    <label for="">Email</label>
                    <input type="text" name="email" class="form-control mb-3">

                    <label for="">Username</label>
                    <input type="text" name="username" class="form-control mb-3">

                    <button class="btn btn-primary">Register</button>
                </form>

            </div>
        </div>
    </div>

</body>
</html>
