<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class UpdateServer implements MessageComponentInterface {
    protected $clients;
    protected $colors;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->colors[ $conn->resourceId ] = $this->getRandomColor();
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        $this->processMessage($from, $msg);

    }

    private function processMessage($from, $msg) {
        $msg = json_decode($msg);
        if ($msg !== null) {
            switch($msg->action) {
                case 'new' :
                    $this->doUpdate($from, $msg);
                    break;
            }
        }
    }

    private function doUpdate($from, $msg) {
        $this->broadcastMessage($from, $msg);
    }

    private function broadcastMessage($from, $message) {
        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send(json_encode($message));
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    private function getRandomColor() {
        $letters = '0123456789ABCDEF';
        $color = '#';
        for ($i = 0; $i < 6; $i++) {
            $color .= substr($letters,floor(rand(0,15)),1);
        }
        return $color;
    }
}