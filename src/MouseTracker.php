<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class MouseTracker implements MessageComponentInterface {
    protected $colors = [];
    protected $names = [];
    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        // echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
        //     , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        echo $msg;

        $msg = json_decode($msg);
        $msg->id = $from->resourceId;
        $msg->color = $this->colors[$from->resourceId];

        switch ($msg->action) {
            case 'add' :
                $this->addMouse($msg, $from);
                break;
            case 'move' :
                $this->broadcastMove($msg, $from);
                break;
            case 'remove' :
                $this->removeMouse($from);
                break;
        }

    }

    private function removeMouse(ConnectionInterface $from) {
        $this->broadcast([
            'action' => 'remove',
            'id' => $from->resourceId 
        ], $from);

        unset($this->colors[$from->resourceId]);
        unset($this->names[$from->resourceId]);
    }

    private function addMouse($msg, ConnectionInterface $from) {
        echo "adding a new mouse\n";
        if (!in_array($msg->name, $this->names)) {
            $this->names[$from->resourceId] = $msg->name;
            $this->colors[$from->resourceId] = $this->getRandomColor();

            $from->send(json_encode([
                'action' => 'added',
                'id' => $from->resourceId,
                'name' => $msg->name,
                'color' => $this->colors[$from->resourceId],
            ]));
        } else {
            $from->send(json_encode([
                'action' => 'add-error',
                'message' => 'Name already in used. Please pick a different name.'
            ]));
        }
    }

    private function broadcastMove($msg, ConnectionInterface $from) {
        echo "broadcasting move from ". $from->resourceId."\n";
        $this->broadcast([
            'action' => 'move',
            'name' => $this->names[$from->resourceId],
            'color' => $this->colors[$from->resourceId],
            'x' => $msg->x,
            'y' => $msg->y,
            'id' => $from->resourceId 
        ], $from);
    }

    public function onClose(ConnectionInterface $conn) {
        $this->broadcast([
            'action' => 'remove',
            'id' => $conn->resourceId 
        ], $conn);

        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);
        unset($this->colors[$conn->resourceId]);
        unset($this->names[$conn->resourceId]);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    private function getRandomColor() {
        $letters = '0123456789ABCDEF';
        $color = '#';
        for ($i = 0; $i < 6; $i++) {
            $color .= substr($letters,floor(rand(0,15)),1);
        }
        return $color;
    }

    private function broadcast($msg, ConnectionInterface $skip = null) {
        foreach ($this->clients as $client) {
            if ($skip !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send(json_encode($msg));
            }
        }
    }
}