<?php
require dirname(__DIR__) . '/vendor/autoload.php';

$app = new \Ratchet\App("127.0.0.1", 7890, '0.0.0.0');

$app->route('/mousetracker', new \MyApp\MouseTracker, array('*'));
$app->route('/newuser', new \MyApp\UpdateServer, array('*'));

$app->run();