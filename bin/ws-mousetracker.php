<?php
require dirname(__DIR__) . '/vendor/autoload.php';

$server = \Ratchet\Server\IoServer::factory(
    new \Ratchet\Http\HttpServer(
        new \Ratchet\WebSocket\WsServer(
            new \MyApp\MouseTracker() // --- aplikasi pemproses di sini
        )
    ),
    7000 // --- port untu WebSocket
);

$server->run();