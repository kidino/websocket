## Aplikasi Contoh WebSocket & WebRTC

Sebelum menggunakan mana-mana aplikasi, pastikan telah melaksanakan arahan `composer`

* `composer install`

# Aplikasi Mouse Tracker

**Mouse Tracker** ini akan memaparkan bulat-bulat kecil yang bergerak mengikut pengguna yang sedang berada di laman tersebut pada masa yang sama.

Aplikasi ini mempunyai fail-fail berikut :

### WebSocket Server

* `/bin/ws-mousetracker.php`

### Frontend Display

* `/public/mousetracker.html`
* `/public/mousetracker-local.html` -- untuk kegunaan localhost

### Cara Menggunakan

1. Lancarkan websocket `php bin/ws-mousetracker.php`
2. Sekiranya di komputer localhost, lancarkan `mousetracker-local.html`
3. Sekiranya di server, lancarkan `mousetracker.html`

# Aplikasi Pendaftaran Pengguna

Aplikasi Pendaftaran Pengguna adalah simulasi di mana senarai penguna dalam Admin Panel akan bertambah apabila ada pengguna baru yang telah mendaftar.

### WebSocket Server

* `/bin/updateserver.php`

### Frontend Display

* `/public/update-ui.php`
* `/public/update-ui-local.php` -- untuk kegunaan localhost
* `/public/update-ws.html`
* `/public/update-ws-local.html` -- untuk kegunaan localhost

### Cara Menggunakan

1. Lancarkan websocket `php bin/updateserver.php`
2. Sekiranya di komputer localhost, lancarkan 
    * `update-ws-local.html`
    * `update-ui-local.php`
3. Sekiranya di server, lancarkan 
    * `update-ws.html`
    * `update-ui.php`

# Aplikasi WebRTC

Aplikasi contoh WebRTC ini tidak menggunakan WebSocket tetapi mengguna Ajax dan Server Sent Event (SSE) dalam proses _Signaling_. Maka WebSocket tidak perlu dilancarkan.

Ini adalah salinan projek oleh https://github.com/nielsbaloe/webrtc-php

### Cara Menggunakan

1. Lancarkan di komputer alamat `localhost/websocket/public/webrtc/index.html`<br>_atau apa-apa alamat yang sesuai mengikut tetapan komputer masing-masing_

Selamat maju jaya.

